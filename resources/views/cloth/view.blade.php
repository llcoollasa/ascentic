<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Product Code</th>
            <th>Short Description</th>
            <th>Cost</th>
            <th>Selling Price</th>
            <th>Brand</th>
            <th>Color</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->product_code }}</td>
                <td>{{ $product->short_description }}</td>
                <td>{{ $product->cost }}</td>
                <td>{{ $product->selling_price }}</td>
                <td>{{ $product->brand }}</td>
                <td>{{ $product->color }}</td>
                <td>{{ $product->size }}</td>
                <td> <a href="{{url('/cloth')}}/{{ $product->id }}" >Edit</a> </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>