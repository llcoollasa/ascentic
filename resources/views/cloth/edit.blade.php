@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Edit {{ $product->name }}</h2>

        @if(Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <div class="row">
            <form method="post" action="{{url('/cloth')}}/{{ $product->id }}">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"  value="{{ $product->name }}" />
                </div>
                <div class="form-group">
                    <label for="product_code">Product Code:</label>
                    <input type="text" class="form-control" name="product_code"  value="{{ $product->product_code }}"/>
                </div>
                <div class="form-group">
                    <label for="short_description">Short Description:</label>
                    <textarea cols="5" rows="5" class="form-control" name="short_description">{{ $product->short_description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="cost">Cost:</label>
                    <input type="text" class="form-control" name="cost" value="{{ $product->cost }}"/>
                </div>
                <div class="form-group">
                    <label for="selling_price">Selling Price:</label>
                    <input type="text" class="form-control" name="selling_price" value="{{ $product->selling_price }}"/>
                </div>
                <div class="form-group">
                    <label for="brand">Brand:</label>
                    <input type="text" class="form-control" name="brand" value="{{ $product->brand }}"/>
                </div>
                <div class="form-group">
                    <label for="color">Color:</label>
                    <input type="text" class="form-control" name="color"  value="{{ $product->color }}"/>
                </div>
                <div class="form-group">
                    <label for="size">Size:</label>
                    <input type="text" class="form-control" name="size" value="{{ $product->size }}"/>
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
                <button onclick="location.href='{{ url('home') }}'"type="button" class="btn btn-default">Back</button>
            </form>


        </div>

        <div class="row">
            <form action="{{url('/cloth')}}/{{ $product->id }}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </div>
    </div>
@endsection