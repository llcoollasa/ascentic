<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Product Code</th>
            <th>Short Description</th>
            <th>Cost</th>
            <th>Selling Price</th>
            <th>Brand</th>
            <th>Gift Amount</th>
            <th>Expiry Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->product_code }}</td>
                <td>{{ $product->short_description }}</td>
                <td>{{ $product->cost }}</td>
                <td>{{ $product->selling_price }}</td>
                <td>{{ $product->brand }}</td>
                <td>{{ $product->gift_amount }}</td>
                <td>{{ $product->expiry_date }}</td>
                <td> <a href="{{url('/giftcard')}}/{{ $product->id }}" >Edit</a> </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>