@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Create Perfume</h2>

        @if(Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <div class="row">
            <form method="post" action="{{url('/perfume')}}">
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"  value="{{ old('name') }}" />
                </div>
                <div class="form-group">
                    <label for="product_code">Product Code:</label>
                    <input type="text" class="form-control" name="product_code"  value="{{ old('product_code') }}"/>
                </div>
                <div class="form-group">
                    <label for="short_description">Short Description:</label>
                    <textarea cols="5" rows="5" class="form-control" name="short_description">{{ old('short_description') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="cost">Cost:</label>
                    <input type="text" class="form-control" name="cost" value="{{ old('cost') }}"/>
                </div>
                {{--<div class="form-group">
                    <label for="selling_price">Selling Price:</label>
                    <input type="text" class="form-control" name="selling_price" value="{{ old('selling_price') }}"/>
                </div>--}}
                <div class="form-group">
                    <label for="brand">Brand:</label>
                    <input type="text" class="form-control" name="brand" value="{{ old('brand') }}"/>
                </div>
                <div class="form-group">
                    <label for="origin_country">Origin Country:</label>
                    <input type="text" class="form-control" name="origin_country"  value="{{ old('origin_country') }}"/>
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
                <button onclick="location.href='{{ url('home') }}'"type="button" class="btn btn-default">Back</button>

            </form>
        </div>
    </div>
@endsection