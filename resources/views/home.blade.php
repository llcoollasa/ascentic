@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">Add Product</div>

                <div class="card-body">

                    <div>
                        <button id="add-clothe" class="btn btn-success"
                                onclick="location.href='{{ URL::route('cloth.create') }}'" >Cloth</button>
                        <button id="add-gift-card" class="btn btn-success"
                                onclick="location.href='{{ URL::route('giftcard.create') }}'" >Gift Card</button>
                        <button id="add-perfume" class="btn btn-success"
                                onclick="location.href='{{ URL::route('perfume.create') }}'" >Perfume</button>
                    </div>

                </div>

            </div>

            <div style="margin-top:10px;"></div>

            <div class="card">
                <div class="card-body">

                    <div>
                        <button id="list-clothes" class="btn btn-dark">Clothes</button>
                        <button id="list-gift-cards" class="btn btn-dark">Gift Cards</button>
                        <button id="list-perfumes" class="btn btn-dark">Perfumes</button>
                    </div>

                    <div id="results" class="mt-4">
                        Products will be listed here for the selected category
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
@endsection
