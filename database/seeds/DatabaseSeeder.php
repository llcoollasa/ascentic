<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ClothesTableSeeder::class);
        $this->call(GiftCardsTableSeeder::class);
        $this->call(PerfumesTableSeeder::class);
    }
}
