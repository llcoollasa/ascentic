<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\GiftCard::class, function (Faker $faker) {

    $unixTimestamp = '1602460800';
    $brands = ['xyz', 'abc', 'aaa', 'bbb', 'ccc'];

    return [
        'name' => $faker->word,
        'product_code' => $faker->ean8,
        'short_description' => $faker->paragraph,
        'cost' => $faker->numberBetween(10,500),
        'selling_price' => $faker->numberBetween(501,5000),
        'brand' => array_random($brands),
        'gift_amount' => $faker->numberBetween(10,500),
        'expiry_date' => $faker->dateTimeBetween('now', $unixTimestamp)
    ];
});
