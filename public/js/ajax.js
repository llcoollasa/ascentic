$(document).ready(function(){

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('#list-clothes').click(function () {
        $.ajax({
            url: '/cloth/',
            type: 'GET',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#results').html(data.html);
            }
        });
    });

    $('#list-gift-cards').click(function () {
        $.ajax({
            url: '/giftcard/',
            type: 'GET',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#results').html(data.html);
            }
        });
    });

    $('#list-perfumes').click(function () {
        $.ajax({
            url: '/perfume/',
            type: 'GET',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function (data) {
                $('#results').html(data.html);
            }
        });
    });


});