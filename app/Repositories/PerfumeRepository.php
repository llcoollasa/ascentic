<?php
namespace App\Repositories;

use App\Models\Perfume;
use App\Repositories\Interfaces\PerfumeInterface;

class PerfumeRepository extends BaseRepository implements PerfumeInterface
{
    function __construct(Perfume $perfume)
    {
        parent::__construct($perfume);
    }

    public function store($data)
    {
        $data['selling_price'] = $this->calculateSellingPrice($data['origin_country'], $data['cost']);
        parent::store($data);
    }

    public function calculateSellingPrice($country, $cost)
    {
        if ($country === 'AU' && $cost >0 ) {
            $sellingPrice =  $cost * 2;
            return $sellingPrice;
        }

        if ($country === 'NZ' && $cost >0 ) {
            $sellingPrice = ($cost + ($cost * 15 / 100)) + 500;
            return $sellingPrice;
        }

        if ($cost >0 ) {
            $sellingPrice = $cost + 1000;
            return $sellingPrice;
        }

        return 0;
    }
}