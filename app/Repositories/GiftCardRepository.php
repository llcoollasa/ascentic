<?php
namespace App\Repositories;

use App\Models\GiftCard;
use App\Repositories\Interfaces\GiftCardInterface;
use App\Repositories\Interfaces\ProductTypeInterface;

class GiftCardRepository extends BaseRepository implements GiftCardInterface
{
    function __construct(GiftCard $giftCard)
    {
        parent::__construct($giftCard);
    }

    public function store($data)
    {
        $data['selling_price'] = $this->calculateSellingPrice($data['brand'], $data['cost']);
        parent::store($data);
    }

    public function calculateSellingPrice($brand, $cost)
    {
        if ($brand === 'abc' && $cost >0 ) {
            $sellingPrice =  $cost + ($cost * 15 / 100);
            return $sellingPrice;
        }

        if ($brand === 'xyz' && $cost >0 ) {
            $sellingPrice = ($cost + ($cost * 15 / 100)) + 100;
            return $sellingPrice;
        }

        if ($cost >0 ) {
            $sellingPrice = $cost + ($cost * 10 / 100);
            return $sellingPrice;
        }

        return 0;
    }
}