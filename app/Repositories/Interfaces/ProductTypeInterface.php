<?php

namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\BaseInterface;

interface ProductTypeInterface{

    public function calculateSellingPrice($brand, $cost);

}