<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

interface BaseInterface
{
    public function find($id);

    public function findAll();

    public function store($data);

    public function update(Request $request, $id);

    public function destroy($id);

}