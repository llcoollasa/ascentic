<?php

namespace App\Repositories\Interfaces;


interface ClothInterface extends ProductTypeInterface {

    public function calculateSellingPrice($brand, $cost);

}