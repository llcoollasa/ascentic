<?php

namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\BaseInterface;

interface GiftCardInterface extends ProductTypeInterface {

    public function calculateSellingPrice($brand, $cost);

}