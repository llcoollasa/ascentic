<?php

namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\BaseInterface;

interface PerfumeInterface extends ProductTypeInterface {

    public function calculateSellingPrice($country, $cost);

}