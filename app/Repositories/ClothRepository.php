<?php
namespace App\Repositories;

use App\Models\Cloth;
use App\Repositories\Interfaces\ClothInterface;

class ClothRepository extends BaseRepository implements ClothInterface
{
    function __construct(Cloth $cloth)
    {
        parent::__construct($cloth);
    }

    public function store($data)
    {
        $data['selling_price'] = $this->calculateSellingPrice($data['brand'], $data['cost']);
        parent::store($data);
    }

    public function calculateSellingPrice($brand, $cost)
    {
        if ($brand === 'abc' && $cost >0 ) {
            $sellingPrice =  $cost + ($cost * 15 / 100);
            return $sellingPrice;
        }

        if ($brand === 'xyz' && $cost >0 ) {
            $sellingPrice = ($cost + ($cost * 15 / 100)) + 100;
            return $sellingPrice;
        }

        if ($cost >0 ) {
            $sellingPrice = $cost + ($cost * 10 / 100);
            return $sellingPrice;
        }

        return 0;
    }
}