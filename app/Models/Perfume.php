<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perfume extends Model
{

    public $rules = [
        'name' => 'required|string',
        'product_code' => 'required|string',
        'short_description' => 'string',
        'cost' => 'numeric',
        'selling_price' => 'numeric',
        'brand' => 'string',
        'origin_country' => 'string',
    ];


    public $timestamps = false;

    public function getFields()
    {
        return array_keys($this->rules);
    }
}
