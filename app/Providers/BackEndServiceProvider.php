<?php

namespace App\Providers;

use App\Repositories\ClothRepository;
use App\Repositories\GiftCardRepository;
use App\Repositories\Interfaces\ClothInterface;
use App\Repositories\Interfaces\GiftCardInterface;
use App\Repositories\Interfaces\PerfumeInterface;
use App\Repositories\PerfumeRepository;
use Illuminate\Support\ServiceProvider;

class BackEndServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ClothInterface::class, ClothRepository::class);
        $this->app->bind(GiftCardInterface::class, GiftCardRepository::class);
        $this->app->bind(PerfumeInterface::class, PerfumeRepository::class);

    }
}
