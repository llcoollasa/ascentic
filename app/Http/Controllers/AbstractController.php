<?php

namespace App\Http\Controllers;

use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Validator;


class AbstractController extends Controller
{
    protected $resource;

    public function __construct(BaseRepository $repo)
    {
        $this->resource = $repo;
    }

    public function index()
    {
        return $this->resource->findAll();
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }


        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        $data = array();

        foreach ($this->resource->getFields() as $field) {
            $data[$field] = $request->input($field);
        }
        try {

            $result = $this->resource->store($data);
            return \Redirect::back()->with('message', 'Successfully Created');

        } catch (\Exception $ex) {

            return \Redirect::back()->withErrors($ex->getMessage())->withInput();

        }
    }

    public function create()
    {
        return view('user.create');
    }

    public function destroy($id)
    {
        $this->resource->destroy($id);
        return \Redirect::back()->with('message', 'Successfully Deleted');
    }

    public function show($id)
    {
        return $this->resource->find($id);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            //return $validator->errors()->all();
            return \Redirect::back()->withErrors($validator)
                ->withInput();
        }

        try{

            $this->resource->update($request, $id);
            return redirect("user/")->with('message', 'Updated Successfully');

        }catch (Exception $ex){

            return redirect("user/")->with('error', $ex->getMessage());

        }


    }

    public function edit($id)
    {
        return $this->resource->find($id);
    }
}
