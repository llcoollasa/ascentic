<?php

namespace App\Http\Controllers;

use App\Models\perfume;
use App\Repositories\Interfaces\PerfumeInterface;
use App\Repositories\Interfaces\ProductTypeInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PerfumeController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PerfumeInterface $perfume)
    {
        $this->middleware('auth');
        $this->resource = $perfume;
    }

    public function index()
    {
        $perfumes = $this->resource->findAll();
        $returnHTML = view('perfume.view')->with('products', $perfumes)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function create()
    {
        return view('perfume.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $data = array();

        foreach ($this->resource->getFields() as $field) {
            $data[$field] = $request->input($field);
        }

        try {

            // save data
            $this->resource->store($data);

            return \Redirect::back()->with('message', 'Successfully Created');

        } catch (\Exception $ex) {

            return \Redirect::back()->withErrors($ex->getMessage())->withInput();

        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)
                ->withInput();
        }

        try{

            $this->resource->update($request, $id);
            return redirect("/perfume/$id")->with('message', 'Updated Successfully');

        }catch (Exception $ex){

            return redirect("/perfume/$id")->with('error', $ex->getMessage());

        }
    }

    public function show($id)
    {
        $perfume =  parent::edit($id);

        return view('perfume.edit', [
            'product' => $perfume
        ]);
    }
}
