<?php

namespace App\Http\Controllers;

use App\Models\Cloth;
use App\Repositories\Interfaces\ClothInterface;
use App\Repositories\Interfaces\ProductTypeInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClothController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClothInterface $cloth)
    {
        $this->middleware('auth');
        $this->resource = $cloth;
    }

    public function index()
    {
        $clothes = $this->resource->findAll();
        $returnHTML = view('cloth.view')->with('products', $clothes)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function create()
    {
        return view('cloth.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $data = array();

        foreach ($this->resource->getFields() as $field) {
            $data[$field] = $request->input($field);
        }

        try {

            // save data
            $this->resource->store($data);

            return \Redirect::back()->with('message', 'Successfully Created');

        } catch (\Exception $ex) {

            return \Redirect::back()->withErrors($ex->getMessage())->withInput();

        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->resource->getRules());

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)
                ->withInput();
        }

        try{

            $this->resource->update($request, $id);
            return redirect("/cloth/$id")->with('message', 'Updated Successfully');

        }catch (Exception $ex){

            return redirect("/giftcard/$id")->with('error', $ex->getMessage());

        }
    }

    public function show($id)
    {
        $cloth =  parent::edit($id);

        return view('cloth.edit', [
            'product' => $cloth
        ]);
    }
}
