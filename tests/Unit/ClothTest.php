<?php

namespace Tests\Unit;

use App\Models\Cloth;
use App\Repositories\ClothRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClothTest extends TestCase
{

    use RefreshDatabase;

    public function testSellingPriceForBrandABC()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => 'abc',
            'cost' => 100,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(115, $clothUpdated->selling_price);
    }

    public function testSellingPriceForBrandXYZ()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => 'xyz',
            'cost' => 100,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(215, $clothUpdated->selling_price);
    }

    public function testSellingPriceForOtherBrands()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => 'sample',
            'cost' => 100,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(110, $clothUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndOtherBrands()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => 'sample',
            'cost' => 0,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(0, $clothUpdated->selling_price);
    }

    public function testSellingPriceForEmptyBrand()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => '',
            'cost' => 100,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(110, $clothUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndEmptyBrand()
    {
        $cloth = new  Cloth();
        $clothRepo = new ClothRepository($cloth);
        $clothRepo->store(factory(Cloth::class)->make([
            'brand' => '',
            'cost' => 0,
        ])->getAttributes());

        $clothUpdated = Cloth::find($clothRepo->getResource()->id);
        $this->assertEquals(0, $clothUpdated->selling_price);
    }

    public function testClothesCanBeSaved(){
        factory(Cloth::class, 2)->create();

        $clothes = Cloth::all();

        $this->assertEquals(2, $clothes->count());
    }
}
