<?php

namespace Tests\Unit;

use App\Models\GiftCard;
use App\Repositories\GiftCardRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GiftCardTest extends TestCase
{

    use RefreshDatabase;

    public function testSellingPriceForBrandABC()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => 'abc',
            'cost' => 100,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(115, $GiftCardUpdated->selling_price);
    }

    public function testSellingPriceForBrandXYZ()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => 'xyz',
            'cost' => 100,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(215, $GiftCardUpdated->selling_price);
    }

    public function testSellingPriceForOtherBrands()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => 'sample',
            'cost' => 100,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(110, $GiftCardUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndOtherBrands()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => 'sample',
            'cost' => 0,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(0, $GiftCardUpdated->selling_price);
    }

    public function testSellingPriceForEmptyBrand()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => '',
            'cost' => 100,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(110, $GiftCardUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndEmptyBrand()
    {
        $GiftCard = new  GiftCard();
        $GiftCardRepo = new GiftCardRepository($GiftCard);
        $GiftCardRepo->store(factory(GiftCard::class)->make([
            'brand' => '',
            'cost' => 0,
        ])->getAttributes());

        $GiftCardUpdated = GiftCard::find($GiftCardRepo->getResource()->id);
        $this->assertEquals(0, $GiftCardUpdated->selling_price);
    }

    public function testGiftCardsCanBeSaved(){
        factory(GiftCard::class, 2)->create();

        $giftCards = GiftCard::all();

        $this->assertEquals(2, $giftCards->count());
    }
}
