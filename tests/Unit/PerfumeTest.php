<?php

namespace Tests\Unit;

use App\Models\Perfume;
use App\Repositories\PerfumeRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PerfumeTest extends TestCase
{

    use RefreshDatabase;

    public function testSellingPriceForAU()
    {
        $Perfume = new Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => 'AU',
            'cost' => 100,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(200, $PerfumeUpdated->selling_price);
    }

    public function testSellingPriceForNZ()
    {
        $Perfume = new  Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => 'NZ',
            'cost' => 100,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(615, $PerfumeUpdated->selling_price);
    }

    public function testSellingPriceForOtherCountries()
    {
        $Perfume = new  Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => 'sample',
            'cost' => 100,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(1100, $PerfumeUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndOtherCountries()
    {
        $Perfume = new  Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => 'sample',
            'cost' => 0,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(0, $PerfumeUpdated->selling_price);
    }

    public function testSellingPriceForEmptyCountry()
    {
        $Perfume = new  Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => '',
            'cost' => 100,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(1100, $PerfumeUpdated->selling_price);
    }

    public function testSellingPriceForZeroCostAndEmptyCountry()
    {
        $Perfume = new  Perfume();
        $PerfumeRepo = new PerfumeRepository($Perfume);
        $PerfumeRepo->store(factory(Perfume::class)->make([
            'origin_country' => '',
            'cost' => 0,
        ])->getAttributes());

        $PerfumeUpdated = Perfume::find($PerfumeRepo->getResource()->id);
        $this->assertEquals(0, $PerfumeUpdated->selling_price);
    }

    public function testPerfumesCanBeSaved(){
        factory(Perfume::class, 2)->create();

        $Perfumes = Perfume::all();

        $this->assertEquals(2, $Perfumes->count());
    }
}
