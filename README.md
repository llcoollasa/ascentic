1. git clone https://llcoollasa@bitbucket.org/llcoollasa/ascentic.git.
2. Create database called `sample` and change the  user name and password in .env.
3. migrate DB using `php artisan migrate`.
4. Seed using `php artisan db:seed`.
5. `composer dum-autoload` in case there were issues.
6. Please open Database and select a user email from `users` table.
7. `php artisan serve` to initiate the application.
8. browser the site ex: http://127.0.0.1:8000.
9. log in using selected email and the password is `secret`.
10. `vendor/bin/phpunit` to run the test cases under unitTest directory.